import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyJspdfComponent } from './my-jspdf.component';

describe('MyJspdfComponent', () => {
  let component: MyJspdfComponent;
  let fixture: ComponentFixture<MyJspdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyJspdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyJspdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
