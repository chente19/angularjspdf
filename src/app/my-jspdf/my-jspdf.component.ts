import { Component, OnInit } from '@angular/core';
import * as jsPDF from 'jspdf';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

@Component({
  selector: 'app-my-jspdf',
  templateUrl: './my-jspdf.component.html',
  styleUrls: ['./my-jspdf.component.sass']
})
export class MyJspdfComponent implements OnInit {

  // you need convert your image in base64
  dataImage = 'iVBORw0KGgoAAAANSUhEUgAAAWgAAABrCAYAAABaBhjPAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz AAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABdkSURB VHic7d15cBzVnQfwb899ag4dlmRZsiXZ8n3b+AArXOYwxhzmqHihwmazB8uyCWGzAWpJEbbCQrIb woYQKgdJIFlgNwkQbGNsbOP7vm/rlizJ8kijkebsc/+QNVL33LJktaTfp8pVfj1ven4jW9/pef36 NfPkpj9KIIQQojqa4S6AEEJIfBTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQ hBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCi UhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUhTQhBCiUrrhLoAQMvJxPg7B+iDC zRFwHTy0vBFlpUUwZGmhtWsAmwTWzCNsZsFpheEud8SggCaEpC18OYRgfQjsFQ5cuwDBrwEj6KBh 9AAYACYAAA/g/LHLcfdhsRhgyzLBkmWAxaWHwaEF7BJ4qwDWxCNkYK/b+1E7CmhCiAzfzSHYEESk jQXbxoPzAQynASMawTAa9MRGT3RogZ5czkAwyCIYZIHW+I/r9VpkOcywu019AW4GJIuIiIVD0MJC kqRreIcjBwU0IWNU6qNhIwBjTwgDGQfxQHGcgHaPH+0ef9zHdTotHM6eADfZdNDbtNBmaSDZBUQs HEJmFiJGR4BTQBMyivU/GuY6ePCdEqSwdtCOhocDzycPcK1WA6vVAJvTBKvTCJNTJwvwsImDwIjX ueqBoYAmZBTg2iLoqvYj0sqC9wFSiAF4fczRMHO1NRKCeKAEQURXVxhdXWGgIfZxjYaBw2mGw2WG 2WmAIUsHxiaBsQGCVYTfGALHqONEJgU0IaOA7YwWRaaJCGQFENKHEQ6ziER4hMMCIhFAFLWpdzJG iKIEb0cQ3o5gwj4WiwHOHAusTiOMNi10Nk30RGbIyoLV8telVgpoQkYJvV4Pp9MJpzP2MY5jEQyG EAgEEQxGwLIiWBYIhxlI0ig+nB6gYJBFsIGNewQOAFabCe4cK2YuLELJlFys7zowJHVQQBMyBuj1 BjgcBjgcjpjHwuEQ/P4AQiEWkQiLSEREJCKBZTUU3v0UFbkxe3YJSkvHIT/fBaPRiOhkEgpoQshQ MJnMMJnMMdtFUUQoFLr6h0UoFEEoJIHjJPD86A5vl8uKGTOKMGNGMQoL3TCbTVAO3F+PmX4U0ISQ uDQaDaxWK6xWa8xjgiAiGAzA7w9Ex7sjEYBlAZ4fWStI2GwmzJ1bgoqKQhQUuGG3W8Aw6hizp4Am hGRMq9XAbrfDbrfHPMZxLDieQ0F+LgRBgt8fhsfjR1OTD6HQ8F4lqNdrUVFRiBkzilBcnAun0w69 Xj+sNSVDAU0IGVR6vQF6vQE+X98siZwcK3JyrDAYdDCZDAAYcBwPvz8CrzeAhgYvIpHBnxnRO25c UVGInBwnDAb9dRmaGCwU0ISQ64ZlebBsXxBrtT3hnZdng8lkgF6vhST1hLfXG8Llyz60tHSldWm3 223DvHmTMG3aeOTmOoZt3HgwUUATQoadKEoIBiOybSaTBiUlLpSWZsNiMUKj0YDnBYRCHDo6AsjL c2Pq1ELk57tgtVowGq++oYAmhKiaIIjo7g7JtjmdJtx11wJoNOo4mTdURtbpVkIIGUMooAkhRKVU P8Rx+dxFNJ84NeDn2/PHoXzFspjtdfsOwdvQKNuWVViAshuXpNxnZ1Mzavfsj9k+58F7odGm/sol CgIuHT+Fml370F5Tj0B7ByKBAHRGI6zZbuRPr8DEGxaiaP5sMMzwj6sd//OnEDku2p67dg0YzdB8 tvMRFvX7D6Fqxx54Gy8h2OEFo9HA4nIiu7QE5SuWo3jhXGh01/Zf11Ndi5pd+9B88gyC3k4EvZ3Q 6HQwO7KQN6UcRfNno+zGJdAZjRntV+QFNB45jqovd6O9pg5BbydEQYDF5YSzaDzKVizFpKWLMt4v GZtUH9DNJ05h76/eG/DzJyyYEzegL27fhYvbdsq2mbLsmLRkETS65CF74uMNOPXJxpjts9asShnQ dfsOYeebv0R325WYx9hAEMEOL65crMbJjzcgKz8PNzyxDpMrlwPDGNQH3/0AbKBvytScB1YPSUC3 nDqLLa+9ge7LbTGPhTp9aK+tx4UvdsBdMgG3/es/I6dsUsav0VHfiC/feBstp87GfTzs64K3oQnn t2zHzjd/ibkP3ou5a9dAZzSk3nddA7a8+hN4aupi99vVjY76RtTs3gdbbg5uffYpjJ87K+P6ydhC Qxz9hLu60XT0eNI+kiiiZufejPctiSK2/ueb2PC9V+KGczxdrW3Y/MqPUX/waMavN9JU79iDj579 t7jhrNRR34j/e/q7uHQ8s29WZzZsxodPfjthOCuxgSAO/O59HHzvg5R9W06fw//+03fihrOS/4oH n3z3JVz4YkdadZCxS/VH0AarFVkF42K2c6EwQp2+aFtvNsPszIrpZ3G5Mnq9qh17ULxofsLHLx0/ haC3M6N9SpKEbT/+Gc59vk223VlUiPLK5cieWAyj3Q42GEBnYzPq9h1C67kL0UmbkjgyFhcfKE9V Lb740U9lc10dhfmYsWolXMUTIEkiPNV1OLP+c/g97QAAkeex6d9/hLVvvBr3/4fS2U1bsf2Nt2UT YU2OLJSvWIb8aRUwuxwQWA7+titoOHwMjUeOQ2B7hnUkIfnP33/Fg8++/1q0PwBY3C7MWLUSueWl 0Gg18DZewun1n6Ozqblnn5KEba//DM6iAuRVTE7/h0XGFNUH9IxVKzFj1cqY7Re37cTm/3g92i6v XIabv/XkgF5Do9XCWVSIjvpG1O45AOFpDtoEl39Wfbk7+vec0olpHTGd+POnsnDW6vVY8tfrMOf+ e+IOXcx/9AG0XajGnl/8Fs0nTmf+hkaYXW+/Az7SNwd20tLFuP25b8mGFSbesBCz19yNDd97Bc0n zwDo+caz753fY+XzzyTdf9uFamx//S1ZOE9deTNuevLr0JtjFwmaee9dCLR34OC7H+DMZ1+krH// b/9HdrCQP60Cq15+Hka7LbqteNF8zFx9J7a89gaqd+wBAAgsh51v/RoPvv5KytcgYxMNcVxVXrkc ABDxB9B4OP4wh8gLqN61D0DPEV5OeWnK/frbPDjwu/f7NjAMbn7mScx5YHXSceW8KWVY89pLWLju odE4/z7q0onTsg8hW042bvvXp+OO+RqsFqx8/hkYbX2L91Tv2ANvQ1PC/YuCgO0/eUv2LWT6Xbfj lm8/FTece1mz3fjKN/8Bd734HejNpoT9fM2tuLi171yG3mzCyheekYVzL61ej1uffQpZ+XnRbZfP XkDDodE/hEUGhgL6qt6ABuRHyf01HjmOSLc/2j+d83YH3n0fXCgcbVfcVokpt6xIqyaGYbD48UdR vHBeWv1HovObt8vasx+4J2lwWtwuTLvztmhbkiSc37I9Yf+LW3fCU1UbbTvGF2DFU3+Tdn2Tli3G gq+uTbz/bTshCn23R5p6+82w5eYk7K8zGjH3wXtl25Q/A0J6UUBf5SwqjM4KqN17EHwkdtWtqu27 on8vizMzRIkLhaNfZwFAo9Ni0bqHM64tnal7I5IkxRw9Tq68MeXTJn9luaxdf+BIwr5nP98qay9c 91DGU/SS/fzrDxyWtcu/krr+ssrlsumTDYePyUKekF4U0P30TsfjQiE0HJL/0gssh9q9PXdNcBYV Iqd0Ysr9Ve/cCy7cN7ZaOGt6Wie0xor2ugYEO7zRti0vB9Ycd8rn5ZRNgt7UN4+4vbY+7onbrpbL 0fFqANCbjCi/KfUHa7oi/gDazldF2xqdDnlTylI+z+zIgmN8Qd9+uv24crFm0OoiowcFdD9llX2/ vFXb5cMc9QePgA32rAfQfzgkGeU0sJJFC66xwtFFOXacW5Z6TB8AGI0G2ZMmyvdV3xjT79KJ07IT g+PnzoLWMHhr/3Y2XpLNPHGXTEh4clkpd7L8vXbEqZ8QCuh+HAX50SOguv2HZWPH/Yc30g3otgtV snZeRfkgVDl69E4562XPz037ufZx8r6dl1pi+ly5WC1rD/Z0ts5LivrzMqhf0den2BchAAV0jN7w 5SOR6PgiF+77u7tkAtwlE1Luh4+wMQHU/2stAXwtrbK2Jd7tqBOwuOXz233NrTF9PNW1srZzkH/+ XS2XFTVlUL8rdf2EUEArlK1YFp3+VvVlzwm+un0Ho2PJ6R49h3w+2dQundEAiyv9X+CxoP/l40DP pfbpUvZV7guAbHwbAOzj8mL6XAvla8abWpeIyZG6fkIooBXsebnInzoFAFB/8DDYYFA2Hl1209K0 9qP8hdObEs+lHav6X5wCIKPxYa1ePhODC4dj+kT88n8DgyXx9L2B4BT16wyp1+vopRyrjlc/Iaq/ knA4lFUuQ+vZ8xBYDhe+2IGGwz1TwbInlcBVXJTWPpQBrUsR0J7qWoS7uhM+nlM6ESZH7KXsI5ly KmMm09+0ijDkw5GYPlxIvsh7sgWPutuuwBdnHLuXPS83ZohK+QGTWf3ygI5XPyEU0HGU37QMe97+ DSRJwr5fvxddYyHd4Q0AMfNaNSlWf9v/mz8knc979/efw8QbFqb9+iOBco2LTFbIU/ZVrlciSRJE xTYmyXzmqu27sfdX7yZ8fO7aNVj2jcflr6GsP4MVBxlGXr+yVkIAGuKIy5rjRv6MqQAQnVoHIO6y pYkov07z7PDebl6NtIojWpFL/67OguLoW2eSr6/MMIxsrjQw+EepyiNygc+gfk5ev7JWQgAK6IQm K46WcyeXZTQLw2CxyNrKr8ME0CsWrRf63RQgFWXfeAFnsA7tv4Fy0X0xk/pZeZinGgIjYxMNcSRQ etNS7Hzr19GvzuUr0js52Et5Rp8NBMFH2ITjoKtefkHWPvL+n7Dvnd9n9JojjXIRIjaY/kwGZd94 AWey2xHwdETbQa8vpk+veQ/fh3kP3xdtt549jz998/mkNcTWH0rQM5ayfjqCJvFQQCdgcTmx6vvP RS9WGT9nZkbPN2XZYXG7olO9JElCV0sr3BOLB73WkcqWJ19UKJN1tpV9lReuAIB74gS019ZH277m FgBzMysyCZviYhPltL5kYurP4CIXMnbQEEcSxYvmo2zFMpStWDagGRS5iuVIO5IsizkWOQrlQ0b9 j3ZTCbTL+zrHF8b0yVFcOh7vcvBrobzwRVlTMsr36iyKrZ8QCughNG6q/NLipiMnhqkSdVKGUv+j 3VTaa+R94wXcOMWl9U3HTmZQXWrKD4X2ugbZ2h/JtNfJ63fE+YAhhAJ6CCmn5TUcPELLSvaTN6VM NibvbWiSrf6XiP+KRzZEYHE5417GXTBrOmw52dF2Z1MzvI2XrrHqPra8HNnQBBsIxl0TREnkebT3 uxOPRqdF/vQpg1YXGT0ooIeQs6gQ+dMqom2/p112942xTmc0onDWjGhbFATU7z+U8nk1u/fL2iWL 58e9Ow3DMJhyW6Vs29EPPxpgtfEp719Zu2d/gp59Gg4elV2kUzBzesysH5KamOa3lZGMAnqIzXvk Pll7/2/+kPFNZ0ez0huXyNonP9mYdJhA5AWc2bBZsY/EM2xmrr5TdoeWC1982bMM6SApu0le/5mN W1JOFzz16WfyfSh+BiRNoz+fKaCH2qSlizFx6aJo2+9px4YXfwD/FU/S542VoZCK2yplt4hqOXUW xz9an7D/gXffl62dnFM+CSWLEt8SzJaTjcWPPxJti4KATS//EC2nziatS+TT+/kXzZstW8bU19yK fb96L2H/Mxs2o+HQsWjb4nZh6spb0notMvaofppdw6FjqNm9L2a7cnnGllNnsf0nP4/p5y6egNn3 rxqy+tKx4qlvwFNdC39bTyi3XajGB3//DGbftwrllcvhKMyHRqeDJIroqG/Euc3bcPLjjcNaczI7 fvqLpDe8TWTp1x+T3fAV6Fk0aNFjj2Dbf70Z3bb75++gs7EZcx64B87xBZAkCR31jTj64Ue4sHVH 35MZBkue+KuUtcxaczcaDh2N3gw43NWNj/7lRUy74xZMubXy6lh4zzxkf5sHNbv34/D7f0z7fS15 Yh3+8txL0cX7j//5U/g97Vjw6IPILi0BwzDwNbfi5CcbceLjDbLnLn7skaRrhJCxTfUB3V5TG/OV Np7OpuaY9ZcBYMKCOcMe0LacbKx57SV8/OyL8HvaAfTcLungex/i4HsfAgCMNhvYQEB2h45ejoJ8 ZKto/vSZjVsG9LxF6x6OCWgAmHbHLbh87oLs3/n0+k04vX4TtHo9JEmMe0S7+LFHULww9bxmjVaL u773Xax/8Qe4dHUmhySKOLNxS/S96M1miDwfd3hCbzahaG7iefBF82bhhq99VXZhUfXOvajeuRca nRYMo4m732l33ILpd9+esv506I2q/1UmA0BDHNeJoyAfa//71YTreUT8/phwZhgGFbdW4qE3fzjo axmrzYp//AbmPLg6ZsEhgeNiwlmj02HJE+uwMMndtpV0RgPuefkFLFz3UNzbUnGhUNwQzZ9WgbVv vBpzMlBp/iP3Y+nXH4tZ0U7khdj9Mgzm3H8PKp/+u7TrTyWwBAhURKAt1MJg16dcnIuMDKr/2LWP y0PRvNkDfr7yYoVe2ROLEbq6X412YP+ZXSUTZLUxmuRftS1uF1a+8G3MXH0nLmzdidq9BxDqjL38 OKtgHIoXzMWsNXenvbzpUBo/e+agrFesXMO5P41Oi+V/+zWULl+CU3/5DHV7D8RMuTPabCi98QbM vm8VsieVZP76Bj0WP/4oKm6txNnPt6Jm176437pMWXaMnzsL0+64Na0jdAAAw2Dew/eheNE8nPx4 I6p37kXE75d10RmNmLRsEWbecycKZk7LuP5kJAbw2gLw2gJAAaABA4ffAlu3GfqAFrxfgCjQinkj DfPkpj+OgXOh6iRJEkKdPgS9nQj7unouD3c5Y27nNBYJHIeApwOB9g4wGg2sOW5Y3W5odImXDB2I SLcfQW8ngt5O6IxGWFwO2PJyM1o6NB6RFxDs8MLvaYckirBku2DLyU77prKDTQPAEbQiy2+GzqcF HxAgjPDAXr36RugyWIN7qITFCH7RlPjE9rWggCZkTGLgCJlg77bA0KUbkUfYwxnQLMehKXAFRwNV aBaSz8i6FsP/8UMIGQYSfOYQfOYQcPX0hi1sgrPLCkO3FmJAAs+Njame6RIEAa2BDpwK1KKKa8L1 +DijgCaEAAD8pjD8prAssLO6LTD69ZC6xTEZ2KIo4krAiwvBJpyI1EC8zlfHUEATQuKKBvbV5UZk ge0XwbOjM7BFSUJnsBvVwWYcDV9EREr/RgyDjQKaEJIWZWCbWQOc3VaYugxAQAIXSf+WX2ojSYA/ HERV4BKOhM8jKKrjFnUU0ISQAQkZWISyWeDqgoFm1ghntwUmvwHwA1x4+I480xUIh1EbaMaR4EX4 pMBwlxODApoQMihChghC2ZFoYJtYPVzdNtUFdpiNoDHQhmPBarQK6d9kYThQQBNChkTYwKEl29sX 2Jwejm4rTH4DNN3MdQ1sjufREmjH8UA16vjW1E9QCQpoQsh1EdZzCLs7AXdPWy9q4eq2wuwzQRtg wAYHN7CHY1rcYKOAJoQMC04joM3RBTi6AAA6Xgt3oC+wuRAfd/GwZCRJgifgw7lAPU6G6yAwI3um CQU0IUQVeJ0isEUt3N1WmLuN0Po14ILxA1uSJHij0+KqEJGuzsC4tqv1VYECmhCiSnz0CLunrRO1 cHbZYPMbwHRr0Bnyoz7SiuOhGgSka1/MS40ooAkhIwKvEeBx+uBx9rRrvZeHt6DrgBaNJYQQlaKA JoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQ laKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlaKAJoQQlfp/ DWu+Cql7fpUAAAAASUVORK5CYII=';

  // document properties
  private orientation: string = "portrait";
  private unit: string = "mm"; // you can replace with: pt, cm, in
  private format: string = "letter";
  private maxLinesPerPage: number = 30; 

  // font properties
  private defaultFontColor: string = '#000000';
  private defaultFontSize: number = 14;
  private fontName: string = 'times'; // the name of your  family font
  private id: string = "prueba"; // id label for use in the document
  // you can check style list with yourOBJjsPDF.getFontList();
  private fontStyleNormal: string = "normal";
  private fontStyleBold: string = "bold";

  // image positions and size in mm
  private xImgPosition = 5;
  private yImgPosition = 10;
  private xImgSize = 100;
  private yImgSize = 30;

  // left text positions
  private xMarginleft = 5;
  private yNextText = 40;
  private ySpacedText = 7;

  // right text positions
  private xMarginRight = 110;
  private yNextTextRight = 27;

  // header table properties
  private colorTextHeader: string = '#FAFAFA';
  private backgroundColorHeader: string = '#689F38';
  private xHeaderSizeTable: number = 205;
  private yHeaderSizeTable: number = 10;
  private header1 = '#';
  private header2 = 'Cliente';
  private header3 = 'Hora de llegada';
  private header4 = 'Duración';
  private header5 = 'Hora de salida';

  // field properties
  private fieldPosition1: number = this.xMarginleft + 5;
  private fieldPosition2: number = this.xMarginleft + 30;
  private fieldPosition3: number = this.xMarginleft + 85;
  private fieldPosition4: number = this.xMarginleft + 130;
  private fieldPosition5: number = this.xMarginleft + 165;

  // for check end rows
  private endRows = false;

  // add new page after first, properties
  private countLines: number = 12;
  private endFirstPage: boolean = false;

  // add new pages after second page



  doc: jsPDF;
  constructor() {
    this.doc = new jsPDF(this.orientation, this.unit, this.format);
    /* let estilos = this.doc.getFontList();
    console.log(estilos); */
    // insert image
    this.loadImageLogo();

    // left text
    this.doc.setFontSize(this.defaultFontSize);
    this.doc.setFont(this.fontName, this.fontStyleNormal);
    this.doc.setTextColor(this.defaultFontColor);
    this.doc.text(this.xMarginleft, this.yNextText, '');
    // use a function to fill new text
    this.addLeftText('Data 1:');
    this.addLeftText('Data 2:');
    // send line break
    this.addLeftText('');
    this.addLeftText('Data 3:');
    this.addLeftText('Data 4:');
    this.addLeftText('Data 5:');
    // send line break
    this.addLeftText('');
    this.addLeftText('Data 6:');
    this.addLeftText('Data 7:');
    this.addLeftText('Data 8:');
    this.addLeftText('Data 9:');

    // Right Text, to line up the text like left text
    this.yNextTextRight = 40;
    this.doc.setFont(this.fontName, this.fontStyleNormal);
    this.doc.text(this.xMarginRight, this.yNextTextRight, '');
    this.addRightText('Data 10:');
    // send break line
    this.addRightText('');
    this.addRightText('');
    this.addRightText('Data 11:');
    this.addRightText('Data 12:');
    this.addRightText('Data 13:');

    // create tables
    this.createTableHeader();
    // create test rows
    for (let i = 0; i < 25; i++) {
      let numero = String(i);
      let cadena = "text test"
      // verify if page the page is full
      this.checkFirstPageLines();
      this.createRow(numero, cadena, cadena, cadena, cadena);
    }    
  }

  ngOnInit() {
  }
  
  resetPositionNewPage() {

    this.doc.setFontSize(this.defaultFontSize);
    // image positions and size in mm
    this.xImgPosition = 5;
    this.yImgPosition = 10;
    this.xImgSize = 100;
    this.yImgSize = 30;
    
    // left text positions
    this.xMarginleft = 5;
    this.yNextText = 40;
    this.ySpacedText = 7;
    
    // right text positions
    this.xMarginRight = 110;
    this.yNextTextRight = 27;
    
    // reset counter lines
    this.countLines = 0;

  }

  appendNewPage() {
    if (this.endFirstPage){
      this.resetPositionNewPage();
      this.doc.addPage();
      if (this.endRows) {
        console.log("TODO: check ehat happenned afther en rows, use this.endRows");
      }
      else {
        this.createTableHeader();
      }
      this.loadImageLogo();
    }
  }

  checkFirstPageLines() {
    if (this.countLines < this.maxLinesPerPage) {
      this.countLines += 1;
    }
    else {
      this.endFirstPage = true;
      this.appendNewPage();
    }
  }

  loadImageLogo() {
    this.doc.addImage(
      this.dataImage,
      'PNG',
      this.xImgPosition,
      this.yImgPosition,
      this.xImgSize,
      this.yImgSize
    );
    // draw subject
    this.doc.setFontSize(18);
    this.doc.setFont(this.fontName, this.fontStyleBold);
    this.doc.text(this.xMarginRight, this.yNextTextRight, 'A subject here');
    // change to normal font size
    this.doc.setFontSize(this.defaultFontSize);
  }

  createRow(id: string, name: string, timeIn: string, totalTime: string, timeOut: string) {
    this.doc.setFontSize(10);
    this.doc.setTextColor(this.defaultFontColor);
    this.doc.setFont(this.fontName, this.fontStyleNormal);
    this.yNextText += this.ySpacedText;
    this.doc.text(this.fieldPosition1, this.yNextText, id);
    this.doc.text(this.fieldPosition2, this.yNextText, name);
    this.doc.text(this.fieldPosition3, this.yNextText, timeIn);
    this.doc.text(this.fieldPosition4, this.yNextText, totalTime);
    this.doc.text(this.fieldPosition5, this.yNextText, timeOut);
  }

  createTableHeader() {
    this.addLeftText('');
    this.doc.setFillColor(this.backgroundColorHeader);
    this.doc.rect(this.xMarginleft, this.yNextText, this.xHeaderSizeTable, this.yHeaderSizeTable, 'F');
    // fill table header
    this.doc.setTextColor(this.colorTextHeader);
    this.yNextText += this.ySpacedText;
    this.doc.text(this.fieldPosition1, this.yNextText, this.header1);
    this.doc.text(this.fieldPosition2, this.yNextText, this.header2);
    this.doc.text(this.fieldPosition3, this.yNextText, this.header3);
    this.doc.text(this.fieldPosition4, this.yNextText, this.header4);
    this.doc.text(this.fieldPosition5, this.yNextText, this.header5);
    // reset font color
    this.doc.setTextColor(this.defaultFontColor);
    this.addLeftText('');
  }

  addLeftText(aText: string) {
    // increment for space line texts in 'y'
    this.yNextText += this.ySpacedText;
    this.doc.text(this.xMarginleft, this.yNextText, aText);
  }

  addRightText(aText: string) {
    this.yNextTextRight += this.ySpacedText;
    this.doc.text(this.xMarginRight, this.yNextTextRight, aText);
  }

  download() {
    this.doc.save('reporte_simply_map.pdf');
  }

}
