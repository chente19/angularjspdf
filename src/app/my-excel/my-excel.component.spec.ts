import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyExcelComponent } from './my-excel.component';

describe('MyExcelComponent', () => {
  let component: MyExcelComponent;
  let fixture: ComponentFixture<MyExcelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyExcelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
