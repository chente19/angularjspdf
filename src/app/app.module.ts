import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MyJspdfComponent } from './my-jspdf/my-jspdf.component';
import { MyExcelComponent } from './my-excel/my-excel.component';
import { MySpreadComponent } from './my-spread/my-spread.component';

@NgModule({
  declarations: [
    AppComponent,
    MyJspdfComponent,
    MyExcelComponent,
    MySpreadComponent
  ],
  imports: [
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
